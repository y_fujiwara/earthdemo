Earth
====

Weather information visualization application demo program for newcomers, written in Java version of Play Framework, ES2015 (and D3.js)

## Description

Have the new employee make a web application as training. 
Policy is to learn how to make traditional web application using template engine. 
Therefore, JavaScript library for UI such as React.js is not used. 
It also aims to make visually fun applications. We use [OpenWeatherMap](https://openweathermap.org/) and D3.js for that. 
In addition, learn about development methods such as TDD and agile development through Play Framework.

This Repository is a demonstration Repository for the above.

## Demo
* demo (not available)
  * Administrator user is admin/admin
  * General user is user/user
  * Note: This demo does not work with User Edit and User Create.

## Requirement

* PostgreSQL 9.5
* Play Framework 2.5
* Java 8
* npm 3.10.9
* OpenWeatherMap Account

## Usage

* Run:
  ```
  $ activator run
  ```
    * Please refer to the official website of activator
      https://www.lightbend.com/community/core-tools/activator-and-sbt

* Client side build

  * Development build:
    ```
    $ npm run build-dev
    ```

  * Release build:
    ```
    $ npm run build-release
    ```

## Install

* Client side package:
  ```
  $ npm install
  ```
* Server side package
  * Automatically resolved when the application starts

## Contribution

1. Fork it ( https://bitbucket.org/y_fujiwara/earthtest/fork )
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request

## Roadmap

* Area Bookmark
* Data Visualize for D3.js
* Delete user and change authority by administrator user
* Selection when there are multiple acquisition results

## Licence

[MIT]()

