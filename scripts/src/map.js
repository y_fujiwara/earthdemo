import L from 'leaflet';

// private用 厳密にはprivateではない
// * 同一ファイル内からは容易に呼び出される
// * getOwnPropertySymbolsからも参照される
const map = Symbol('map');
const init = Symbol('init');
const zoom = Symbol('zoom');

// Symbol対策
import "babel-polyfill";

export default class Map {

    constructor() {
        this[map] = {};
        this[zoom] = 16;
        this[init]();
    }

    [init]() {
        // map要素が無い場合は地図画面ではない
        if (!document.getElementById("map")) {
            console.log("this page is not map page");
            return;
        }

        let std = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        });

        let chiriin = L.tileLayer('https://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png', {
            attribution: "<a href='http://portal.cyberjapan.jp/help/termsofuse.html' target='_blank'>国土地理院</a>"
        });


        let pf = new L.tileLayer('http://tile.openweathermap.org/map/precipitation/{x}/{y}/{z}.png?appid=9f70e15ce517f3eb8a5c50dabd8eaf57', {
            attribution: '<a href="">precipitation</a>',
            opacity: 0.5
        });

        let pr = new L.tileLayer('http://tile.openweathermap.org/map/pressure/{x}/{y}/{z}.png?appid=9f70e15ce517f3eb8a5c50dabd8eaf57', {
            attribution: '<a href="">pressure</a>',
            opacity: 0.5
        });


        let wi = new L.tileLayer('http://tile.openweathermap.org/map/wind/{x}/{y}/{z}.png?appid=9f70e15ce517f3eb8a5c50dabd8eaf57', {
            attribution: '<a href="">wind</a>',
            opacity: 0.5
        });

        let te = new L.tileLayer('http://tile.openweathermap.org/map/temperature/{x}/{y}/{z}.png?appid=9f70e15ce517f3eb8a5c50dabd8eaf57', {
            attribution: '<a href="">tempressure</a>',
            opacity: 0.5
        });

        /*

        let pf = L.OWM.precipitation({ 
          appId: '9f70e15ce517f3eb8a5c50dabd8eaf57',
          attribution: '<a href="">precipitation</a>',
          opacity:0.5
        });

        //pressure 
        let pr = L.OWM.pressure({ 
          appId: '9f70e15ce517f3eb8a5c50dabd8eaf57',
          attribution: '<a href="">precipitation</a>',
          opacity:0.5,
          showLegend: false
        });

        //wind 
        let wi = L.OWM.wind({ 
          appId: '9f70e15ce517f3eb8a5c50dabd8eaf57',
          attribution: '<a href="">precipitation</a>',
          opacity:0.5,
          showLegend: false
        });

        //temperature 
        let te = L.OWM.temperature({ 
          appId: '9f70e15ce517f3eb8a5c50dabd8eaf57',
          attribution: '<a href="">precipitation</a>',
          opacity:0.5, 
          showLegend: false
        });
        */

        this[map] = L.map("map", {
            center: [37.09, 138.52],
            zoom: this[zoom],
            layers: [std]
        });

        //ベースレイヤーグループ化
        let baseMaps = {
            "Mapbox(osm)": std,
            "Mapbox(chiriin)": chiriin
        };

        //オーバレイヤーグループ化
        let overlayMaps = {
            'Precipitation': pf,
            'Pressure': pr,
            'Wind': wi,
            'Temperature': te
        };

        L.control.layers(baseMaps, overlayMaps).addTo(this[map]);

        // スケールバーを追加
        L.control.scale().addTo(this[map]);
    }

    mapPan(lat, lon) {
        this[map].panTo([lat, lon]);
        this[map].setZoom(this[zoom]);
    }
}
