import * as d3 from "d3";
import moment from 'moment';
// Symbol対策
import "babel-polyfill";

const createUrl = Symbol('createUrl');
const dataProps = { 
  "temperature" : { "unit" : "℃",  
                    "convert" :    (d) => { return d.main.temp - 273.15;}, 
                    "convertMin" : (d) => { return d.main.temp - 273.15 - 1;}, 
                    "convertMax" : (d) => { return d.main.temp - 273.15 + 1;}},
  "pressure" :    { "unit" : "hpa", 
                    "convert" :    (d) => { return d.main.pressure; }, 
                    "convertMin" : (d) => { return d.main.pressure - 5; },
                    "convertMax" : (d) => { return d.main.pressure + 5; }},
  "humidity" :    { "unit" : "%",   
                    "convert" :    (d) => { return d.main.humidity; },
                    "convertMin" : (d) => { return d.main.humidity - 5; },
                    "convertMax" : (d) => { return d.main.humidity + 5; }} 
};

export default class Visualize { 
  constructor() { 
    // 必ず最初はmapが表示されているためそれの大きさを取る
    let $container = $("#d3-temperature");
    // TODO: オリジナルを外でconstにする
    this.size = this.sizeOrg = { 
      width: $container.width(),
      height: $container.height()
    };
    this.scale = this.scaleOrg = 1;
    this.aspect = this.aspectOrg = this.size.width / this.size.height;
    this.margin = this.marginOrg = { 
      top   : 40, 
      right : 40, 
      bottom: 40, 
      left  : 40 
    };
    this.currentData = {};
  }

  [createUrl](city) { 
    return 'http://api.openweathermap.org/data/2.5/forecast?q=' + 
      city + 
      '&APPID=9f70e15ce517f3eb8a5c50dabd8eaf57'; 
  }

  getAndDraw(city, data) {
    let url = this[createUrl](city);

    $.ajax({ 
      url: url, 
      dataType: "jsonp", 
    }).then((json) => { 
      this.currentData = json.list; 
      this.drawPoly(data)
    }, (err) => { 
      console.log(err.status + ":" + err.statusText); 
      alert('取得出来ませんでした。'); 
    });
 
  }

  drawPoly(elemName) { 
    let mo = moment;
    let elemId = "#d3-" + elemName;
    let $container = $(elemId);
    $container.empty();

    // svgの準備 高さを幅はレスポンシブにする方法をあとで探すこと
    let w = $container.width(); 
    let h = this.size.height; 

    // svg要素の挿入先はid=d3のエレメント
    let svg = d3
      .select(elemId)
      .append("svg") 
      .attr("width", w) 
      .attr("height", h);
      
    let g = svg.append("g");

    // スケール定義
    // d3.scale.linearで複雑な描画領域に対するフィッティング計算を肩代わりしてくれる
    // y = n × 500（高さ）/41 -14（メダルの最小値、最大値の差）
    let y_scale = d3.scaleLinear() 
      // min,max関数は第二引数に従って要素を精査して最大最小を返す
      .domain([
        d3.min(this.currentData,  dataProps[elemName].convertMin), 
        d3.max(this.currentData,  dataProps[elemName].convertMax)]
      ).range([h,0]); // 描画領域の最大値と最小値を入れる
    
    // どんな呼び出しでもグラフを書く上ではここの処理は同一
    let x_scale = d3.scaleLinear() 
      .domain([d3.min(this.currentData, 
        (d) => { return d.dt;}), 
        d3.max(this.currentData, (d) => { return d.dt;})]
      ).range([0,w]);

    // パス要素の追加 html5 svg要素の機能
    // 折れ線グラフなのでline
    let line = d3.line().curve(d3.curveMonotoneX)
      .x((d,i) => { return x_scale(d.dt); }) // x座標のとり方
      .y((d,i) => { return y_scale(dataProps[elemName].convert(d)); });
    
    let path = g.append("path") 
      .datum(this.currentData) // データバインド
      .attr("d", line) 
      .attr("stroke", "rgb(159, 224, 190)") 
      .attr("fill", "none") 
      .attr("stroke-width", "3px"); 

    // パスの長さを取得
    let pathLength = path.node().getTotalLength() 
    
    // "stroke-dasharray" と "stroke-dashoffset" を使ってパスをアニメーションで描画
    // http://blog.aagata.ciao.jp/?eid=129
    path.attr("stroke-dasharray", pathLength + " " + pathLength)
      .attr("stroke-dashoffset", pathLength)
      .transition()
      .duration(2000)  // 描画総時間の設定
      .ease(d3.easeSin) // 描画の仕方の設定
      .attr("stroke-dashoffset", 0);

    // 点の配置
    let point = g.selectAll(".c")
      .data(this.currentData)
      .enter()
      .append("circle") 
      .attr("cx", line.x()) 
      .attr("fill", "rgb(152, 152, 152)"); 

    point.transition().delay(400) 
      .attr("r", 0) 
      .duration(2000) 
      .delay((d, i) => {  // データの順番に応じて待ち時間をずらす
        return i * 50; 
      })
      .ease(d3.easeBounce) 
      .attr("r", 3)
      .attr("cy", line.y());

    // 軸の生成
    let x_axis = d3.axisBottom().scale(x_scale) 
      .tickFormat((date) => { return  mo(date * 1000).format("YYYY/MM/DD hh:mm");});
    let y_axis = d3.axisLeft().scale(y_scale)
      .tickSizeInner(-w)  // 目盛線の長さ（内側）v4になって名前が変わっていた 以下も同様 https://keithpblog.wordpress.com/2016/07/31/upgrading-d3-from-v3-to-v4/
      .tickSizeOuter(0) // 目盛線の長さ（外側）
      .tickPadding(10) // 目盛線とテキストの間の長さ
      .ticks(5).tickFormat((d) => {return d + dataProps[elemName].unit;}); // ラベルのフォーマット

    // 軸のスケールを図の方と一致させる
    svg.append("g") 
      .classed("x_axis",true) 
      .attr("transform", "translate(0," + h + ")") 
      .call(x_axis)
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("transform", "rotate(-60) translate(-5,-5)")
      .attr("font-size", "12px");
    
    svg.append("g") 
      .classed("y_axis",true) 
      .call(y_axis)
      .selectAll("text")
      .attr("font-size", "13px");
   
    // 点に表示するテキストとその位置 
    g.selectAll(".t").data(this.currentData).enter().append("text") 
      .attr("x", line.x()) 
      .attr("y", (d,i) => { 
        return line.y()(d,i) - 10; 
      })
      // 各点に表示するテキスト
      .text((d) => { return  (dataProps[elemName].convert(d)).toFixed(1) + dataProps[elemName].unit; })
      .attr("font-size", "13px");
  }
}
