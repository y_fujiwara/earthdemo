import Weather from './weather';
import Map from './map';
import Visualize from './visualize';

$(function() { 
  // デバイス判定
  let getDevice = (() => { 
    let ua = navigator.userAgent; 
    if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){ 
      return 'sp'; 
    } else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){ 
      return 'tab'; 
    } else { 
      return 'other'; 
    } 
  })();

  // 初期化処理
  let weather = new Weather();
  let map = new Map();
  let visualize = new Visualize();
  let topBtn = $('#page-top');    
  let searchFixBtn = $('#fixed-search');    
  // グラフ表示のデフォルトは気温
  let selectGraph = "temperature";
  // Current表示されているタブ
  let currentTab = "map";
  const lat = 0;
  const lon = 1;

  // 初回の発火
  weather.getRequest(); 
  visualize.getAndDraw("Tokyo", selectGraph);
  topBtn.hide(); 
  searchFixBtn.hide();

  //スクロールが100に達したらボタン表示
  $(window).scroll( () => { 
    if ($(this).scrollTop() > 100) { 
      topBtn.fadeIn(); 
      searchFixBtn.fadeIn(); 
    } else { 
      topBtn.fadeOut(); 
      searchFixBtn.fadeOut(); 
    } 
  }); 

  // fixed要素のオーバーレイ表示時ガタツキ対策
  // 右寄せ要素が前提になっている
  $("#offcanvas").on("show", (e) => {
    if(getDevice == "sp" || getDevice == "tab") {
      return false;
    }
    $(".fixed-item").css("right", "27px"); 
  });

  // オーバーレイ非表示時はスタイルを元に戻す
  $("#offcanvas").on("hide", (e) => {
    if(getDevice == "sp" || getDevice == "tab") {
      return false;
    }
    $(".fixed-item").removeAttr('style');
    if ($(this).scrollTop() < 100) { 
      topBtn.hide(); 
      searchFixBtn.hide();
    }
  });

  //スクロールしてトップ
  topBtn.click( () => { 
    $('body,html').animate({ 
      scrollTop: 0 
    }, 500); 
    return false; 
  }); 

  let searchExec = (elemId) => { 
    let newCity = $(elemId).val(); 
    weather.setCity(newCity); 
    weather.getRequest();
    // グラフの出し方によるが今のレスポンシブ対応だとやり方を考える必要がある
    visualize.getAndDraw(newCity, selectGraph);
  };
  
  // イベント設定 検索ボタンクリック
  $('#search-city').click( () => {
    searchExec('#input-city');
  });
  $('#fixed-search-city').click( () => {
    searchExec('#fixed-input-city');
  });
  
  // イベント設定 検索ボックスエンターキー押下
  $('#input-city').keypress( (e) => { 
    if ( e.which == 13 ) { 
      searchExec('#input-city');
    } 
  }); 
  $('#fixed-input-city').keypress( (e) => { 
    if ( e.which == 13 ) { 
      searchExec('#fixed-input-city');
    } 
  });

  $("#latlon").change( () => {
    let latlon = $("#latlon").text().split(",");
    map.mapPan(latlon[lat], latlon[lon]);
  });

  $("#map-area").on('show', () => {
    currentTab = "map";
  });

  // グラフタブの親タブをクリックした時はデフォルトで前選択していものをを開く
  $("#d3-li").on('show', (e) => {
    currentTab = "d3-li";
    visualize.drawPoly(selectGraph);
  });

  $("#d3-li-temperature").on('show', () => { 
    selectGraph = "temperature";
    visualize.drawPoly("temperature");
  });

  $("#d3-li-pressure").on('show', () => {
    selectGraph = "pressure";
    visualize.drawPoly("pressure");
  });

  $("#d3-li-humidity").on('show', () => {
    selectGraph = "humidity";
    visualize.drawPoly("humidity");
  });
});

