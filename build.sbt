name := "ttc-earth-demo"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs, 
  filters,
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42",
  "org.mockito" % "mockito-core" % "2.1.0",
  "org.mindrot" % "jbcrypt" % "0.4"
)
