package controllers;

import play.mvc.*;
import play.data.*;
import javax.inject.*;

import views.html.*;

import models.*;
import dto.*;
import services.*;
import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;

public class LoginController extends Controller {
    @Inject Authenticator auth;
    @Inject FormFactory formFactory; 

    @AddCSRFToken
    public Result login() {
        return ok(login.render("Earth Login"));
    }

    @RequireCSRFCheck
    public Result doLogin() {
        LoginRequest request = formFactory.form(LoginRequest.class).bindFromRequest().get();
        Account user = auth.login(request);
        if(user == null){
            flash("message","login failed.");
            return redirect(routes.LoginController.login());
        }
        setSession(user);
        return redirect(routes.HomeController.index());
    }

    public Result logout() {
        clearSession();
        return redirect(routes.HomeController.index());
    }

    private void setSession(Account user) {
        // 変更される可能性がある
        session("fullName",user.fullName);
        // タイプの変更はそのうち発生する
        session("type",user.type.toString());
        session("id",user.id.toString());
    }

    private void clearSession() {
        session().clear();
    }
}
