package controllers;

import java.util.List;
import play.mvc.*;
import play.data.*;
import javax.inject.*;

import views.html.*;

import models.*;
import dto.*;
import services.*;
import play.Configuration;
import authenticator.AdminSecured;
import authenticator.Secured;

import play.filters.csrf.AddCSRFToken;
import play.filters.csrf.RequireCSRFCheck;

public class AccountController extends Controller { 
    @Inject 
    private Configuration configuration;

    @Inject
    FormFactory formFactory; 
    
    @Inject 
    UserManager userManager;

    @Security.Authenticated(AdminSecured.class)
    @AddCSRFToken
    public Result register() {
        Long id =Long.parseLong(session("id")); 
        return ok(register.render(id, formFactory.form(Account.class)));
    }

    @Security.Authenticated(AdminSecured.class)
    @AddCSRFToken
    public Result index() {
        Long id =Long.parseLong(session("id")); 
        List<Account> users = userManager.getAll();
        return ok(userIndex.render("Earth Users", id, users));
    }
    
    @Security.Authenticated(AdminSecured.class)
    @RequireCSRFCheck
    public Result create() { 
        // 変更を許可していない場合は変更されたと見せかけてリダイレクト
        if (!configuration.getBoolean("account.edit")) {
            return redirect(routes.HomeController.index());
        }

        Account user = formFactory.form(Account.class).bindFromRequest().get();

        String errorMessage = userManager.createUser(user);

        if(errorMessage != null){
            flash("errormsg", errorMessage);
            return redirect(routes.AccountController.register());
        }
        return redirect(routes.HomeController.index());
    }

    @Security.Authenticated(Secured.class)
    @AddCSRFToken
    public Result edit(Long id) {
        // セッションID=リクエストIDでないならアクセス不可
        if (Long.parseLong(session("id")) != id) { 
            return redirect(routes.HomeController.index());
        }

        UserUpdateRequest updateTarget = userManager.createUpdateTarget(id);
        if (updateTarget == null) {
            return redirect(routes.HomeController.index());
        }

        return ok(edit.render(id, formFactory.form(UserUpdateRequest.class).fill(updateTarget), formFactory.form(PasswordUpdateRequest.class)));
    }

    @Security.Authenticated(Secured.class)
    @RequireCSRFCheck
    public Result update(Long id) { 
        // 変更を許可していない場合は変更されたと見せかけてリダイレクト
        if (!configuration.getBoolean("account.edit")) {
            return redirect(routes.HomeController.index());
        }

        UserUpdateRequest user = formFactory.form(UserUpdateRequest.class).bindFromRequest().get(); 

        // セッションIDが変更しようとしているユーザのIDなのはeditアクションで確認済み
        String errorMessage = userManager.updateUser(user, id);
        if(errorMessage != null){
            flash("exceptionUser", errorMessage);
            return redirect(routes.AccountController.edit(id));
        }

        session("fullName",user.fullName);
        return redirect(routes.HomeController.index());
    } 
    
    @Security.Authenticated(Secured.class)
    @RequireCSRFCheck
    public Result passwordUpdate(Long id) {
        if (!configuration.getBoolean("account.edit")) {
            return redirect(routes.HomeController.index());
        }

        PasswordUpdateRequest pass = formFactory.form(PasswordUpdateRequest.class).bindFromRequest().get(); 

        // セッションIDが変更しようとしているユーザのIDなのはeditアクションで確認済み
        String errorMessage = userManager.updatePassword(pass, id);

        if(errorMessage != null){
            flash("exceptionPass", errorMessage);
            return redirect(routes.AccountController.edit(id));
        }

        // パスワードを変更した時はログアウト
        return redirect(routes.LoginController.logout());
    }
}
