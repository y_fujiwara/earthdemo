/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.File;
import play.mvc.Controller;
import play.mvc.Result;
import static play.mvc.Results.ok;

/**
 *
 * @author y-fujiwara
 */
public class TopoJsonController extends Controller {

    /**
     *
     * @return
     */
    public Result download() {
        File file = new File("data/topojson/test_data.topojson");
        return ok(file);
    }

}
