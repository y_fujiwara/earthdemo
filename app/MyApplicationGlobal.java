import models.*;
import javax.persistence.*;
import javax.inject.*;
import play.inject.ApplicationLifecycle;
import org.mindrot.jbcrypt.BCrypt;
import play.Configuration;

@Singleton
public class MyApplicationGlobal {

    @Inject
    public MyApplicationGlobal(ApplicationLifecycle lifecycle) {
        Account user = Account.find.where().eq("type",0).findUnique();
        if (user == null) { 
            try { 
                Account newUser = new Account();
                String hashedPassword = BCrypt.hashpw("admin", BCrypt.gensalt(8)); 
                newUser.password = hashedPassword; 
                newUser.userName = "admin";
                newUser.fullName = "admin";
                // 初期データは管理ユーザ
                newUser.type = 0;
                newUser.save(); 
                
                // もう一人は一般ユーザー
                Account newUser2 = new Account();
                String hashedPassword2 = BCrypt.hashpw("user", BCrypt.gensalt(8)); 
                newUser2.password = hashedPassword2; 
                newUser2.userName = "user";
                newUser2.fullName = "user";
                newUser2.type = 1;
                newUser2.save(); 
            } 
            catch (Exception e) { 
                // とりあえず何もしない
            } 
        } 
    } 
}

