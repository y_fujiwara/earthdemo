package models;

import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import com.avaje.ebean.Model;
import play.data.format.*;
import play.data.validation.*;

@Entity
@Table(name = "accounts")
public class Account extends Model {

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @NotNull
    @Column(unique=true)
    public String userName;

    @NotNull
    public String password;

    @NotNull
    public String fullName;

    @Column(columnDefinition = "integer default 1")
    public Integer type = 1;

    @Column(columnDefinition = "integer default 1")
    public Integer isDelete = 1;

    public static Finder<Long, Account> find = new Finder<Long,Account>(Account.class);
}
