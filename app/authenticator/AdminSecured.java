package authenticator;

import play.mvc.*;
import views.html.*;
import controllers.*;
import play.Logger;

public class AdminSecured extends Security.Authenticator{

    @Override
    public String getUsername(Http.Context ctx){
        // 通常のログインチェック
        String id =  ctx.session().get("id");
        if (id == null ) {
            return null;
        }

        // 更に権限チェック
        String type = ctx.session().get("type");
        if (type == null ) {
            return null;
        }
        return (type.equals("0")) ? type : null;
    }

    @Override
    public Result onUnauthorized(Http.Context ctx){
        Logger.info("unauthorized access : " + ctx.request().remoteAddress());
        return redirect(routes.HomeController.index());
    }
}
