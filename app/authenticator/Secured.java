package authenticator;

import play.mvc.*;
import views.html.*;
import controllers.*;

public class Secured extends Security.Authenticator{

    @Override
    public String getUsername(Http.Context ctx){
        return ctx.session().get("id");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx){
        String returnUrl = ctx.request().uri();
        if (returnUrl == null) {
            returnUrl = "/";
        }
        ctx.session().put("returnUrl", returnUrl);

        return redirect(routes.LoginController.login());
    }

}
