package dto;

// ログイン時に入力される情報とマッピングするクラス
public class LoginRequest {

  public String userName;

  public String password;
}
