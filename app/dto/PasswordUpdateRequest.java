package dto;

// ログイン時に入力される情報とマッピングするクラス
public class PasswordUpdateRequest {

  public String oldPassword;

  public String newPassword;

  public String confirmNewPassword;
}
