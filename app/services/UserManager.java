package services;
import org.mindrot.jbcrypt.BCrypt;
import models.*;
import dto.*;
import javax.inject.*;
import java.util.List;

@Singleton
public class UserManager { 

    public List<Account> getAll() { 
        List<Account> users = Account.find.all();
        return users;
    }
    
    public String createUser(Account user) {

        if (Account.find.where().eq("userName",user.userName).findRowCount() > 0) {
            return "Duplicate user name."; 
        }

        try {
            String hashedPassword = BCrypt.hashpw(user.password, BCrypt.gensalt(8));
            user.password = hashedPassword;
            user.save();
        } catch(Exception e) {
            return "Unexpected error. Please give it time.";
        }
        return null;
    }

    public UserUpdateRequest createUpdateTarget(Long id) {
        Account user = Account.find.byId(id);
        if (user == null ) {
            return null;
        }

        UserUpdateRequest ret = new UserUpdateRequest();
        ret.userName = user.userName;
        ret.fullName = user.fullName;
        return ret;
    }

    public String updateUser(UserUpdateRequest user, Long id) {
        // 自分自身があるので1より大きい場合はNG
        if (Account.find.where().ne("id", id).eq("userName",user.userName).findRowCount() > 0) { 
            return "Duplicate user name."; 
        }
        
        Account currentUser = Account.find.byId(id);
        try {
            currentUser.userName = user.userName;
            currentUser.fullName = user.fullName;
            currentUser.update();
        } catch(Exception e) {
            return "Unexpected error. Please give it time.";
        }
        return null;
    }

    public String updatePassword(PasswordUpdateRequest pass, Long id) { 
        Account user = Account.find.byId(id);

        if (user == null) {
            return "User not found.";
        }

        if (!BCrypt.checkpw(pass.oldPassword, user.password)) {
            return "Old password isn't valid.";
        } 

        if (!pass.newPassword.equals(pass.confirmNewPassword)) {
            return "Password doesn't match the confirmation.";
        }
        
        try {
            user.password = BCrypt.hashpw(pass.newPassword, BCrypt.gensalt(8));
            user.update();
        } catch(Exception e) {
            return "Unexpected error. Please give it time.";
        }
        return null;
    }
}
