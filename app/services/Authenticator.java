package services;

import models.*;
import javax.persistence.*;
import javax.inject.*;
import dto.*;
import org.mindrot.jbcrypt.BCrypt;

@Singleton
public class Authenticator {
    public Account login(LoginRequest req){
        Account user = Account.find.where().eq("userName",req.userName).findUnique();
        // 1件もなかったら即終了
        if (user == null) {
            return null;
        }
        // 1件あったらパスワードチェック
        else if (BCrypt.checkpw(req.password, user.password)) {
            // 認証成功
            return user;
        }
        return null;
    }
}
